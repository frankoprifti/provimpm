package com.example.provim;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    String arrayList[]=new String[]{"Matematike","Fizike"};
    String arrayList1[]=new String[]{"Matematike Description","Fizike Description"};
    Boolean isShowing=false;
    int selected=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Spinner s = findViewById(R.id.spinner);
        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayList);
        s.setAdapter(arrayAdapter);
        final TextView lendName=findViewById(R.id.lenda_emri);
        final TextView lendDesc=findViewById(R.id.lenda_desc);
        final EditText emri=findViewById(R.id.name);
        final EditText email=findViewById(R.id.email);
        final EditText programi=findViewById(R.id.programi);

        lendName.setAlpha(0); lendDesc.setAlpha(0);
        Button btn=findViewById(R.id.info);

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected=i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v){
                isShowing=true;
                lendDesc.setAlpha(1);lendName.setAlpha(1);
                lendName.setText(arrayList[selected]);
                lendDesc.setText(arrayList1[selected]);
            }
        });
        Button submit=findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                SharedPreferences preferences=getSharedPreferences("Emri",MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();

            String stored=preferences.getString(emri.getText().toString(),"");
                Integer lendeStored=preferences.getInt(arrayList[selected],-1);
            if(stored.isEmpty()){
                Toast.makeText(MainActivity.this,emri.getText()+" - "+arrayList[selected], Toast.LENGTH_SHORT).show();
                editor.putString(emri.getText().toString(),arrayList[selected]);
                if(lendeStored!=-1){
                    editor.putInt(arrayList[selected],lendeStored+1);
                }else{
                    editor.putInt(arrayList[selected],1);
                }
                editor.commit();
            }else{
                Toast.makeText(MainActivity.this,"Je i regjistruar me pare!", Toast.LENGTH_SHORT).show();
            }


        }

        });
  }
}
